import dis
import inline_nasm
import ctypes

def compile_fn(fn):
    if not hasattr(fn, "func_code"):
        return fn(*args)
    fn_co = fn.func_code

    gen_code = ["""
    bits 64

    start:"""]
    def emit(code):
        gen_code.append(code)

    functype = ctypes.CFUNCTYPE(ctypes.c_double, *([ctypes.c_double]*fn_co.co_argcount))

    def get_loc(idx):
        return "qword [rbp-%d]" % ((idx+1)*8)

    def get_const(idx):
        return "const_%d" % idx

    def get_target(i):
        return "opcode_%d" % i

    emit("""
    push rbp
    mov rbp,rsp
    sub rsp,%d""" % ((fn_co.co_nlocals*8)))

    for i in range(min(fn_co.co_argcount, 8)):
        emit("movsd [rbp%+d],xmm%d" % (-(i+1)*8,i))
    for i in range(8, fn_co.co_argcount):
        emit("""
        mov rax,qword[rbp%+d]
        mov qword[rbp%+d],rax
        """ % ((i-8-3)*8, i*8))

    loop_stack = []

    bytecode = fn_co.co_code
    n = len(bytecode)
    i = 0

    om = dis.opmap
    ret = None

    while i<n:
        orig_i = i
        op = ord(bytecode[i])
        i+=1
        oarg = None
        if op >= dis.HAVE_ARGUMENT:
            oarg = ord(bytecode[i]) | (ord(bytecode[i+1])<<8)
            i+=2

        emit("%s:   ; %s %r" % (get_target(orig_i), dis.opname[op], oarg))
        #print i, op
        if op == om["STOP_CODE"]:
            emit("int3")
        elif op == om["POP_TOP"]:
            # 1 =>
            emit("pop rax")
        elif op == om["ROT_TWO"]:
            # 1 2 => 2 1
            emit("""
            pop rax
            pop rcx
            push rax
            push rcx""")
            s[-1], s[-2] = s[-2], s[-1]
        elif op == om["ROT_THREE"]:
            # 1 2 3 => 2 3 1
            emit("""
            pop rax
            pop rcx
            pop rdx
            push rax
            push rdx
            push rcx""")
        elif op == om["ROT_FOUR"]:
            # 1 2 3 4 => 2 3 4 1
            s[-1],s[-2],s[-3],s[-4] = s[-2],s[-3],s[-4],s[-1]
            emit("""
            pop rax
            pop rcx
            pop rdx
            pop rsi
            push rax
            push rsi
            push rdx
            push rcx
            """)
        elif op == om["DUP_TOP"]:
            # 1 => 1 1
            emit("""
            pop rax
            push rax
            push rax""")
        elif op == om["NOP"]:
            pass
        elif op == om["UNARY_POSITIVE"]:
            emit("; UNARY_POSITIVE")
        elif op == om["UNARY_NEGATIVE"]:
            emit("xor byte [rsp],0x80")
        elif op in (om["BINARY_POWER"], om["INPLACE_POWER"]):
            assert(False)
            #exp = s.pop()
            #base = s.pop()
            #s.append(base**exp)
        elif op in (om["BINARY_MULTIPLY"], om["INPLACE_MULTIPLY"]):
            emit("""
            movsd xmm0,[rsp+8]
            mulsd xmm0,[rsp]
            add rsp,8
            movsd [rsp],xmm0
            """)
        elif op in (om["BINARY_DIVIDE"], om["INPLACE_DIVIDE"], om["BINARY_TRUE_DIVIDE"], om["INPLACE_TRUE_DIVIDE"]):
            emit("""
            movsd xmm0,[rsp+8]
            divsd xmm0,[rsp]
            add rsp,8
            movsd [rsp],xmm0
            """)
        elif op in (om["BINARY_MODULO"], om["INPLACE_MODULO"]):
            assert(False)
            #b = s.pop()
            #a = s.pop()
            #s.append(a%b)
        elif op in (om["BINARY_ADD"], om["INPLACE_ADD"]):
            emit("""
            movsd xmm0,[rsp+8]
            addsd xmm0,[rsp]
            add rsp,8
            movsd [rsp],xmm0
            """)
        elif op in (om["BINARY_SUBTRACT"], om["INPLACE_SUBTRACT"]):
            emit("""
            movsd xmm0,[rsp+8]
            addsd xmm0,[rsp]
            sub rsp,8
            movsd [rsp],xmm0
            """)
        elif op in (om["BINARY_FLOOR_DIVIDE"], om["INPLACE_FLOOR_DIVIDE"]):
            assert(False)
            #b = s.pop()
            #a = s.pop()
            #s.append(a//b)
        elif op == om["BREAK_LOOP"]:
            target = loop_stack.pop()
            emit("jmp %s" % get_target(target))
        elif op == om["RETURN_VALUE"]:
            emit("""
            movsd xmm0,[rsp]
            jmp eof""")
            break
        elif op == om["POP_BLOCK"]:
            loop_stack.pop()
        elif op == om["DUP_TOPX"]:
            # 1 2 ... oarg => 1 2 ... oarg 1 2 ... oarg
            assert(False)
            #s.extend(s[-oarg:])
        elif op == om["LOAD_CONST"]:
            emit("push qword [rel %s]" % get_const(oarg))
        elif op == om["COMPARE_OP"]:
            b = s.pop()
            a = s.pop()
            if oarg == 0:
                r = a<b
            elif oarg == 1:
                r = a<=b
            elif oarg == 2:
                r = a==b
            elif oarg == 3:
                r = a!=b
            elif oarg == 4:
                r = a>b
            elif oarg == 5:
                r = a>=b
            else:
                raise NotImplementedError("COMPARE_OP(%d)" % oarg)
            s.append(r)
        elif op == om["JUMP_FORWARD"]:
            emit("jmp %s" % (get_target(i+oarg)))
        elif op == om["JUMP_IF_FALSE_OR_POP"]:
            c = s[-1]
            if not c:
                i = oarg
            else:
                s.pop()
        elif op == om["JUMP_IF_TRUE_OR_POP"]:
            c = s[-1]
            if c:
                i = oarg
            else:
                s.pop()
        elif op == om["JUMP_ABSOLUTE"]:
            i = oarg
        elif op == om["POP_JUMP_IF_FALSE"]:
            c = s.pop()
            if not c:
                i = oarg
        elif op == om["POP_JUMP_IF_TRUE"]:
            c = s.pop()
            if c:
                i = oarg
        elif op == om["SETUP_LOOP"]:
            loop_stack.append(i+oarg)
        elif op == om["LOAD_FAST"]:
            emit("""
            mov rax,%s
            push rax""" % get_loc(oarg))
        elif op == om["STORE_FAST"]:
            emit("""
            pop rax
            mov %s,rax""" % get_loc(oarg))
        elif op == om["DELETE_FAST"]:
            pass
        elif op == om["CALL_FUNCTION"]:
            assert(False)
            argsn = oarg & 0xff
            kwargsn = oarg >> 8
            if kwargsn:
                raise NotImplementedError("kwargs are not supported")
            args = s[-argsn:]
            del s[-argsn:]
            target = s.pop()
            s.append(interp_fn(target, args))
        else:
            raise NotImplementedError(i, dis.opname[op])

    emit("""
    eof:
        mov rsp,rbp
        pop rbp
        ret
    consts:""")
    for idx,co in enumerate(fn_co.co_consts):
        if co is None:
            co = 0.
        emit("%s: dq %r" % (get_const(idx), float(co)))

    gc = '\n'.join(gen_code)
    print gc
    return inline_nasm.asm(functype, gc)
